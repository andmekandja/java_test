package com.cgi.dentistapp.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.cgi.dentistapp.dto.TimesDTO;
import org.codehaus.groovy.runtime.powerassert.SourceText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

@Service
@Transactional
public class DentistVisitService {

    @Autowired
    private DentistVisitDao dentistVisitDao;

    private static final int MAX_NR_OF_TIMES_IN_DAY = 9;
    private static final List<String> DENTIST_LIST = new ArrayList<>(
            Arrays.asList("Dr Noorman", "Dr Strangelove", "Dr Who"));
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private DateTimeFormatter amFormatter = DateTimeFormatter.ofPattern("HH:mm");

    public void addVisit(String dentistName, String visitDate, String visitTime, String clientName) {
        DentistVisitEntity visit = new DentistVisitEntity(dentistName, LocalDate.parse(visitDate, formatter),
                LocalTime.parse(visitTime, amFormatter), clientName);
        dentistVisitDao.create(visit);
    }

    public List<DentistVisitEntity> listVisits () {
        return dentistVisitDao.getAllVisits();
    }

    public List<String> getDentistList() {
        return DENTIST_LIST;
    }

    public TimesDTO getDates(String name) {
        TimesDTO dto = new TimesDTO();
        dto.setName(name);
        dentistVisitDao.getVisitsByName(name).forEach(entity -> addToTimesDTO(entity,dto));
        dto.getDateTimes().entrySet().stream().forEach(entry -> disableDates(entry.getKey(), dto));
        return dto;
    }

    private void addToTimesDTO(DentistVisitEntity entity, TimesDTO dto) {
        dto.addDateTimes(entity.getVisitDate(), entity.getVisitTime());
    }

    private void disableDates(LocalDate date, TimesDTO dto) {
        if (dto.getDateTimes().get(date).size() >= MAX_NR_OF_TIMES_IN_DAY) {
            dto.setDisabledDates(date);
        }
    }

    public TimesDTO getTimes(TimesDTO dto) {
            List<DentistVisitEntity> entities = dentistVisitDao.getVisitsByNameAndDate(dto.getName(),
                    LocalDate.parse(dto.getChosenDate(), formatter));
            List<String> times = new ArrayList<>();
            entities.stream()
                    .map(DentistVisitEntity::getVisitTime)
                    .forEach(localTime -> times.add(amFormatter.format(localTime.withMinute(0))));
            dto.setDisabledTimes(times);
        return dto;
    }

}
