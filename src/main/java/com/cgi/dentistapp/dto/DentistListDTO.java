package com.cgi.dentistapp.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class DentistListDTO {

    private List<String> dentists;
    private List<LocalDateTime> bookedTimes;
    private String selectedDentist;
    private String selectedDate;
    private String selectedTime;
    private String clientName;
    private String clientEmail;
    private String clientTelNr;
    private String additionalInfo;

    public List<String> getDentists() {
        return dentists;
    }

    public void setDentists(List<String> dentists) {
        this.dentists = dentists;
    }

    public List<LocalDateTime> getBookedTimes() {
        return bookedTimes;
    }

    public void setBookedTimes(List<LocalDateTime> bookedTimes) {
        this.bookedTimes = bookedTimes;
    }

    public String getSelectedDentist() {
        return selectedDentist;
    }

    public void setSelectedDentist(String selectedDentist) {
        this.selectedDentist = selectedDentist;
    }

    public String getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(String selectedDate) {
        this.selectedDate = selectedDate;
    }

    public String getSelectedTime() {
        return selectedTime;
    }

    public void setSelectedTime(String selectedTime) {
        this.selectedTime = selectedTime;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientTelNr() {
        return clientTelNr;
    }

    public void setClientTelNr(String clientTelNr) {
        this.clientTelNr = clientTelNr;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }
}
