package com.cgi.dentistapp.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class TimesDTO {
    String name;
    List<LocalDate> disabledDates = new ArrayList<>();
    List<String> disabledTimes = new ArrayList<>();
    HashMap<LocalDate, ArrayList<LocalTime>> dateTimes = new HashMap();
    String chosenDate;

    public void setDisabledDates(List<LocalDate> disabledDates) {
        this.disabledDates = disabledDates;
    }

    public HashMap<LocalDate, ArrayList<LocalTime>> getDateTimes() {
        return dateTimes;
    }

    public void addDateTimes(LocalDate date, LocalTime time) {
        if (this.dateTimes.containsKey(date)) {
            this.dateTimes.get(date).add(time);
        } else {
            this.dateTimes.put(date, new ArrayList<LocalTime>(Arrays.asList(time)));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LocalDate> getDisabledDates() {
        return disabledDates;
    }

    public void setDisabledDates(LocalDate disabledDate) {
        this.disabledDates.add(disabledDate);
    }

    public String getChosenDate() {
        return chosenDate;
    }

    public void setChosenDate(String chosenDate) {
        this.chosenDate = chosenDate;
    }

    public List<String> getDisabledTimes() {
        return disabledTimes;
    }

    public void setDisabledTimes(List<String> disabledTimes) {
        this.disabledTimes = disabledTimes;
    }
}
