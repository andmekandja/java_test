package com.cgi.dentistapp.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

import java.time.LocalDate;
import java.util.List;

@Repository
public class DentistVisitDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(DentistVisitEntity visit) {
        entityManager.persist(visit);
    }

    public List<DentistVisitEntity> getAllVisits() {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e")
                .getResultList();
    }

    public List<DentistVisitEntity> getVisitsByName(String name) {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e WHERE e.dentistName LIKE :custName")
                .setParameter("custName", name)
                .getResultList();
    }

    public List<DentistVisitEntity> getVisitsByNameAndDate(String name, LocalDate date) {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e WHERE e.dentistName LIKE :custName AND e.visitDate = :date")
                .setParameter("custName", name)
                .setParameter("date", date)
                .getResultList();
    }
}
