package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.dto.DentistListDTO;
import com.cgi.dentistapp.dto.TimesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistVisitService dentistVisitService;


    @GetMapping("/")
    public String showRegisterForm(DentistListDTO dentistListDTO) {
        dentistListDTO.setDentists(dentistVisitService.getDentistList());
        return "form";
    }

    @PostMapping("/")
    public String postRegisterForm(DentistListDTO dentistListDTO, Model model) {
        model.addAttribute("info", dentistListDTO);
        return "info";
    }

    @PostMapping("/getDates")
    public String getDates(@RequestBody  String name, Model model) {
        TimesDTO dto = dentistVisitService.getDates(name);
        model.addAttribute("disabled", dto.getDisabledDates());
        return "form :: content";
    }

    @PostMapping("/getTimes")
    public String getTimes(TimesDTO dto, Model model) {
        dto = dentistVisitService.getTimes(dto);
        model.addAttribute("disabledTimes", dto.getDisabledTimes());
        return "form :: content";
    }

    @GetMapping("/bookings")
    public String getBookings(Model model) {
        model.addAttribute("visits", dentistVisitService.listVisits());
        return "bookings";
    }

    @PostMapping("/submit")
    public String submit(DentistListDTO dto) {
        System.out.println(dto.getSelectedDentist());
        dentistVisitService.addVisit(dto.getSelectedDentist(), dto.getSelectedDate(), dto.getSelectedTime(), dto.getClientName());
        return "redirect:/success";
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/success").setViewName("success");
    }

}
